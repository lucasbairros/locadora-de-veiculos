@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <menu-component></menu-component>
                        <br>
                        <br>
                        <div class="text-center">
                            <h5>
                               <u>
                                   Bem-vindo
                               </u>
                            </h5>
                            <p>
                                Utilize o menu conforme sua preferencia!
                            </p>
                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
