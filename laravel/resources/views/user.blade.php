@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Cadastro de usuários') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <menu-component></menu-component>
                        <br>
                        <user-create-component></user-create-component>
                        <br>
                        <user-list-component :id="{{$id}}"/>
                        <br>
                        <spinner-component></spinner-component>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
