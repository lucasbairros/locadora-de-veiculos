/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


// As a plugin
import VueMask from 'v-mask'

Vue.use(VueMask);

// Or as a directive
import {VueMaskDirective} from 'v-mask'

Vue.directive('mask', VueMaskDirective);

// Or only as a filter
import {VueMaskFilter} from 'v-mask'

Vue.filter('VMask', VueMaskFilter)


import swal from 'sweetalert';

Vue.filter('swal', swal)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('menu-component', require('./components/MenuComponent.vue').default);
Vue.component('user-create-component', require('./components/UserCreateComponent.vue').default);
Vue.component('user-list-component', require('./components/UserListComponent.vue').default);
Vue.component('spinner-component', require('./components/SpinnerComponent.vue').default);
Vue.component('vehicle-create-component', require('./components/VehicleCreateComponent.vue').default);
Vue.component('vehicle-list-component', require('./components/VehicleListComponent.vue').default);
Vue.component('reservation-component', require('./components/ReservationComponent.vue').default);
Vue.component('reservation-list-component', require('./components/ReservationListComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
