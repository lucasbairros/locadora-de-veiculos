<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class VehicleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('vehicle', ['id' => Auth::id()]);
    }

}
