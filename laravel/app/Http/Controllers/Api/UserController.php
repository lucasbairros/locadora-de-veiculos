<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public static function index()
    {
        return response()->json(User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->validator($request['form'])):
            $user = new User(
                [
                    'name' => $request['form']['name'],
                    'email' => $request['form']['email'],
                    'cpf' => preg_replace("/\D+/", "", $request['form']['cpf']),
                    'password' => Hash::make($request['form']['password']),
                ]
            );
            $user->save();

            $status = true;
            $message = 'Success';
        else:
            $status = false;
            $message = 'Error ao tentar salvar!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validator($request['form'])):
            $user = User::where('id', $id);
            $user->update(
                [
                    'name' => $request['form']['name'],
                    'email' => $request['form']['email'],
                    'cpf' => preg_replace("/\D+/", "", $request['form']['cpf']),
                    'password' => Hash::make($request['form']['password']),
                ]
            );
            $status = true;
            $message = 'Success!';
        else:
            $status = false;
            $message = 'Error!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id);

        if ($user->delete()):
            $status = true;
            $message = 'Success';
        else:
            $status = false;
            $message = 'Error!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Validator method.
     *
     */
    public function validator($form)
    {
        return Validator::make($form, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'cpf' => ['required', 'string', 'email', 'max:11', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

}
