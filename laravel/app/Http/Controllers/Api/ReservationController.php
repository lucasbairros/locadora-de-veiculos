<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ReservationController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $r = Reservation::join('vehicles', 'reservations.vehicle_id', '=', 'vehicles.id')
            ->join('users', 'reservations.user_id', '=', 'users.id')
            ->select(
                [
                    'reservations.id',
                    'reservations.reserved_at',
                    'reservations.user_id',
                    'reservations.vehicle_id',
                    'vehicles.brand',
                    'vehicles.model',
                    'vehicles.year',
                    'vehicles.license_plate',
                    'users.name'
                ]
            )
            ->orderBy('reservations.reserved_at')
            ->get();

        return response()->json($r);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->validator($request['form'])):
            $reservation = new Reservation(
                [
                    'user_id' => $request['form']['user_id'],
                    'vehicle_id' => $request['form']['vehicle_id'],
                    'reserved_at' => $request['form']['ano'] . '-' . $request['form']['mes'] . '-' . $request['form']['dia'],
                ]
            );
            $reservation->save();
            $status = true;
            $message = 'Success';

        else:
            $status = false;
            $message = 'Error!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validator($request['form'])):
            $reservation = Reservation::where('id', $id);
            $reservation->update(
                [
                    'user_id' => $request['form']['user_id'],
                    'vehicle_id' => $request['form']['vehicle_id'],
                    'reserved_at' => $request['form']['ano'] . '-' . $request['form']['mes'] . '-' . $request['form']['dia'],
                ]
            );
            $status = true;
            $message = 'Success';
        else:
            $status = false;
            $message = 'Error!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reservation = Reservation::where('id', $id);
        if ($reservation->delete()):
            $status = true;
            $message = 'Success';
        else:
            $status = false;
            $message = 'Error!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function comboboxMonth(Request $request)
    {
        $numberDays = cal_days_in_month(CAL_GREGORIAN, (int)$request['form']['mes'], (int)$request['form']['ano']);

        $daysOff[] = [
            'value' => null,
            'text' => 'Selecione'
        ];

        for ($day = 1; $day <= $numberDays; $day++):

            $v = Reservation::join('vehicles', 'reservations.vehicle_id', '=', 'vehicles.id')
                ->where('reservations.vehicle_id', $request['form']['vehicle_id'])
                ->where('reserved_at', '=', $request['form']['ano'] . '-' . $request['form']['mes'] . '-' . $day)
                ->get()->count();

            if (!$v):
                $daysOff[] = [
                    'value' => $day,
                    'text' => str_pad($day, 2, '0', STR_PAD_LEFT)
                ];
            endif;

        endfor;

        return response()->json($daysOff);
    }

    /**
     * Validator method.
     *
     */
    public function validator($form)
    {
        return Validator::make($form, [
            'user_id' => ['required', 'string', 'user_id'],
            'vehicle_id' => ['required', 'string', 'vehicle_id'],
            //'reserved_at' => ['required', 'string', 'reserved_at', 'max:11', 'unique:users'],
        ]);
    }

}
