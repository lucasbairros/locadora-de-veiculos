<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VehicleController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public static function index()
    {
        return response()->json(Vehicle::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->validator($request['form'])):
            $vehicle = new Vehicle($request['form']);
            $vehicle->save();
            $status = true;
            $message = 'Success';
        else:
            $status = false;
            $message = 'Error!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validator($request['form'])):
            $vehicle = Vehicle::where('id', $id);
            $vehicle->update($request['form']);
            $status = true;
            $message = 'Success';
        else:
            $status = false;
            $message = 'Error!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle = Vehicle::where('id', $id);
        if ($vehicle->delete()):
            $status = true;
            $message = 'Success';
        else:
            $status = false;
            $message = 'Error!';
        endif;

        return response()->json(['status' => $status, 'message' => $message]);
    }

    /**
     * Validator method.
     *
     */
    public function validator($form)
    {
        return Validator::make($form, [
            'brand' => ['required', 'brand', 'max:50'],
            'model' => ['required', 'string', 'model', 'max:50', 'unique:vehicles'],
            'year' => ['required', 'year', 'email', 'max:5', 'unique:vehicles'],
            'license_plate' => ['required', 'string', 'min:7', 'confirmed'],
        ]);
    }

}
