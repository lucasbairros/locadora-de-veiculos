<?php

namespace App\Listeners;

use App\Events\ReservationLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendReservationNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param ReservationLog $event
     * @return void
     */
    public function handle(ReservationLog $event)
    {
        //
    }
}
