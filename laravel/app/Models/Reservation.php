<?php

namespace App\Models;

use App\Events\ReservationLog;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $table = 'reservations';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'vehicle_id',
        'reserved_at'
    ];

    /*
     * My Envent
     **/
    protected $dispatchesEvents = [
        'creating' => ReservationLog::class,
    ];
}
