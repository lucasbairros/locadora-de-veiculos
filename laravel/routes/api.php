<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


# --- user --- #
Route::get('/user_api', function () {
    return \App\Http\Controllers\Api\UserController::index();
});
Route::post('/user_api', function (Request $request) {
    $user = new \App\Http\Controllers\Api\UserController();
    return $user->store($request);
});
Route::patch('/user_api/{id}', function (Request $request, $id) {
    $user = new \App\Http\Controllers\Api\UserController();
    return $user->update($request, $id);
});
Route::delete('/user_api/{id}', function ($id) {
    $user = new \App\Http\Controllers\Api\UserController();
    return $user->destroy($id);
});


# --- vehicle --- #
Route::get('/vehicle_api', function () {
    return \App\Http\Controllers\Api\VehicleController::index();
});
Route::post('/vehicle_api', function (Request $request) {
    $user = new \App\Http\Controllers\Api\VehicleController();
    return $user->store($request);
});
Route::patch('/vehicle_api/{id}', function (Request $request, $id) {
    $user = new \App\Http\Controllers\Api\VehicleController();
    return $user->update($request, $id);
});
Route::delete('/vehicle_api/{id}', function ($id) {
    $user = new \App\Http\Controllers\Api\VehicleController();
    return $user->destroy($id);
});


# --- reservation --- #
Route::get('/reservation_api', function () {
    $reservation = new \App\Http\Controllers\Api\ReservationController();
    return $reservation->index();
});
Route::post('/reservation_api/mes', function (Request $request) {
    $reservation = new \App\Http\Controllers\Api\ReservationController();
    return $reservation->comboboxMonth($request);
});
Route::post('/reservation_api', function (Request $request) {
    $reservation = new \App\Http\Controllers\Api\ReservationController();
    return $reservation->store($request);
});
Route::patch('/reservation_api/{id}', function (Request $request, $id) {
    $reservation = new \App\Http\Controllers\Api\ReservationController();
    return $reservation->update($request, $id);
});
Route::delete('/reservation_api/{id}', function ($id) {
    $reservation = new \App\Http\Controllers\Api\ReservationController();
    return $reservation->destroy($id);
});

